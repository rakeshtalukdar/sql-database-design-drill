CREATE TABLE `Patients`
(
    `patient_id` INT PRIMARY KEY,
    `firstname` TEXT NOT NULL,
    `lastname` TEXT NOT NULL,
    `date_of_birth` DATE NOT NULL,
    `sex` TEXT NOT NULL,
    `pin` INT NOT NULL,
    `city` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `email` TEXT
);

CREATE TABLE `Doctors`
(
    `doctor_id` INT PRIMARY KEY,
    `firstname` TEXT NOT NULL,
    `lastname` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `city` TEXT NOT NULL,
    `email` TEXT,
    `secretary_id` INT REFERENCES `Secretaries`(`id`)
);

CREATE TABLE `Secretaries`
(
    `secretary_id` INT PRIMARY KEY,
    `firstname` TEXT NOT NULL,
    `lastname` TEXT NOT NULL,
    `city` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `email` TEXT
);

CREATE TABLE `Prescriptions`
(
    `prescription_id` INT PRIMARY KEY,
    `date` DATE NOT NULL,
    `patient_problem` TEXT NOT NULL,
    `doctor_id` INT NOT NULL REFERENCES `Doctors`(`doctor_id`),
    `patient_id` INT  NOT NULL REFERENCES `Patients`(`patient_id`)
);

CREATE TABLE `Drugs`
(
    `drug_id` INT PRIMARY KEY,
    `name` TEXT NOT NULL,
    `expiry_date` DATE NOT NULL,
    `manufacturer` TEXT NOT NULL,
    `price` FLOAT NOT NULL
);


CREATE TABLE `Drugs_Dosage_Prescriptions`
(
    `id` INT PRIMARY KEY,
    `dose` TEXT NOT NULL,
    `duration` TEXT NOT NULL,
    `instructions` TEXT NOT NULL,
    `drug_id` INT NOT NULL REFERENCES `Drugs`(`drug_id`),
    `prescription_id` INT NOT NULL REFERENCES `Prescriptions`(`prescription_id`)
);
