
CREATE TABLE `Authors`
(
    `id` INT PRIMARY KEY,
    `first_name` TEXT NOT NULL,
    `last_name` TEXT NOT NULL,
    `city` TEXT NOT NULL,
    `pin` INT NOT NULL,
    `email` TEXT,
    `phone` TEXT
);

CREATE TABLE `Publishers`
(
    `id` INT PRIMARY KEY,
    `name` TEXT NOT NULL,
    `city` TEXT NOT NULL,
    `pin` INT NOT NULL,
    `phone` TEXT NOT NULL,
    `email` TEXT
);

CREATE TABLE `Books`
(
    `isbn` VARCHAR(255) PRIMARY KEY,
    `title` TEXT NOT NULL,
    `num_of_copies` INT NOT NULL,
    `price` FLOAT NOT NULL,
    `publisher_id` INT NOT NULL REFERENCES `Publishers`(`id`)
);

CREATE TABLE `Authors_Books`
(
    `id` INT PRIMARY KEY,
    `book_isbn` VARCHAR(255) NOT NULL REFERENCES `Books`(`isbn`),
    `author_id` INT NOT NULL REFERENCES `Authors`(`id`)
);

CREATE TABLE `Branches`
(
    `id` INT PRIMARY KEY,
    `name` TEXT NOT NULL,
    `city` TEXT NOT NULL,
    `pin` INT NOT NULL,
    `phone` TEXT
);

CREATE TABLE `Branches_Books`
(
    `id` INT PRIMARY KEY,
    `branch_id` INT NOT NULL REFERENCES `Branches`(`id`),
    `book_isbn` VARCHAR(255) NOT NULL REFERENCES `Books`(`isbn`)
);
