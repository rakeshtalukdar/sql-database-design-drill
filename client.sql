CREATE TABLE `Locations`
(
    `id` INT PRIMARY KEY,
    `pin` INT NOT NULL,
    `city` TEXT NOT NULL
);

CREATE TABLE `Managers`
(
    `id` INT PRIMARY KEY,
    `first_name` TEXT NOT NULL,
    `last_name` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `email` TEXT,
    `location_id` INT NOT NULL REFERENCES `Locations`(`id`)
);

CREATE TABLE `Staff`
(
    `id` INT PRIMARY KEY,
    `first_name` TEXT NOT NULL,
    `last_name` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `email` TEXT,
    `manager_id` INT REFERENCES `Managers`(`id`),
    `location_id` INT NOT NULL REFERENCES `Locations`(`id`)

);

CREATE TABLE `Clients`
(
    `id` INT PRIMARY KEY,
    `first_name` TEXT NOT NULL,
    `last_name` TEXT NOT NULL,
    `phone` TEXT NOT NULL,
    `email` TEXT,
    `manager_id` INT REFERENCES `Managers`(`id`),
    `location_id` INT NOT NULL REFERENCES `Locations`(`id`)

);

CREATE TABLE `Contracts`
(
    `id` INT PRIMARY KEY,
    `sign_date` DATE NOT NULL,
    `estimated_cost` real NOT NULL,
    `final_cost` FLOAT,
    `completion_date` DATE NOT NULL,
    `client_id` INT REFERENCES `Clients`(`id`),
    `manager_id` INT REFERENCES `Managers`(`id`)
);
